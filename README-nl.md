DATAtourisme is een nationaal project geleid door [ADN tourisme](https://www.adn-tourisme.fr/). ADN tourisme is de nationale federatie van institutionele toeristische organisaties in Frankrijk. 
Dit project bestaat uit een ontologie en een platform voor het verzamelen, standaardiseren en als open data beschikbaar stellen van toeristische gegevens.

Het doel van deze ontologie is **het structureren van beschrijvende gegevens met betrekking tot voor toeristen nuttige locaties (points of interest)** verzameld door de toerismebureaus, departementale agentschappen en regionale comités voor toerisme: feesten en manifestaties, erfgoedsites, vrijetijdsactiviteiten, routes, vakantieverblijven, restaurants, winkels en diensten. Dit referentieformaat wordt door [**het nationale platform DATAtourisme**](https://www.datatourisme.fr/) met name gebruikt om openbare toeristische gegevens voor bestemmingen in Frankrijk als open data beschikbaar te stellen: de gegevens worden in verschillende formaten door het platform ontvangen en vervolgens in overeenstemming gebracht met de ontologie, zodat ze als open data in een homogeen, interoperabel formaat beschikbaar kunnen worden gesteld.
Deze ontologie is bedoeld om relevant te zijn voor elke producent van gegevens, op elk grondgebied, ook buiten Frankrijk, en om op grote schaal te worden gedeeld.

**Documentatie over het DATAtourisme-formaat**
De documentatie is [online beschikbaar](https://www.datatourisme.fr/ontology/core/), en [te downloaden via de Gitlab-repository] (https://gitlab.adullact.net/adntourisme/datatourisme/ontology/). Hierbij hoort ook documentatie in pdf-formaat met concrete voorbeelden en CSV-bestanden van de gebruikte nomenclaturen en vocabulaires.

**Ophalen van gegevens uit DATAtourisme**
De nationale gegevens zijn beschikbaar via het [opendataportaal van de Franse overheid](https://www.data.gouv.fr/fr/datasets/datatourisme-la-base-nationale-des-donnees-du-tourisme-en-open-data/) en via het [distributieplatform van DATAtourisme](https://diffuseur.datatourisme.fr/fr/login). Op dit laatstgenoemde platform kunt u de gegevensuitwisseling tussen DATAtourisme en uw applicaties configureren: zoeken op meerdere criteria, beschikbare formaten JSON, XML, RDF enz.

**Gebruik van gegevens van DATAtourisme**
Voor hergebruik van gegevens vindt u op de [Gitlab-omgeving van DATAtourisme] (https://gitlab.adullact.net/adntourisme/datatourisme/) nog twee andere repository’s. De [API-repository] (https://gitlab.adullact.net/adntourisme/datatourisme/api) bevat een PHP-bibliotheek voor het raadplegen van gegevens van DATAtourisme. De [Docker-stack] (https://gitlab.adullact.net/adntourisme/datatourisme/docker-stack) bevat een technische omgeving waarin gegevens van DATAtourisme in RDF-formaat kunnen worden opgeslagen.

**DATAtourisme-gemeenschap en ondersteuning**
DATAtourisme is een samenwerkingsproject, dat zich door bijdragen van gebruikers verder ontwikkelt. Deel daarom uw opmerkingen, suggesties en voorkeuren via de [ondersteuningswebsite van DATAtourisme](https://support.datatourisme.fr/).
