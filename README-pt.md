A DATAtourisme é uma infraestrutura nacional promovida pela [ADN tourisme] (https://www.adn-tourisme.fr/). ADN tourisme é a Federação Nacional de organizações de turismo institucional em França. 
A infraestrutura consiste numa ontologia e numa plataforma de coleção, padronização e distribuição de dados abertos, aplicadas ao setor do turismo.
Esta ontologia destina-se a **estruturar os dados que descrevem o conjunto de pontos de interesse turístico** identificados pelas delegações de turismo, agências e entidades regionais de turismo: festas e eventos, monumentos e sítios de interesse patrimonial, atividades de lazer, itinerários, alojamentos, restaurantes, lojas e serviços. Este formato de referência é utilizado, nomeadamente, pela [**plataforma nacional DATAtourisme**] (https://www.datatourisme.fr/) para a distribuição em regime aberto de dados públicos de informações turísticas relativas aos destinos franceses: os dados são recebidos na plataforma em diferentes formatos e são depois alinhados com a ontologia, de modo a poderem ser distribuídos num formato uniforme e interoperável como dados abertos.
Esta ontologia destina-se a ser relevante para qualquer produtor de dados, em qualquer território, incluindo fora de França, e a ser amplamente partilhada.

**Documentação sobre o formato DATAtourisme**
A documentação está [disponível em linha] (https://www.datatourisme.fr/ontology/core/) e é [descarregável a partir do repositório Gitlab] (https://gitlab.adullact.net/adntourisme/datatourisme/ontology). Estão disponíveis documentos em formato PDF com exemplos concretos e ficheiros CSV das diferentes nomenclaturas e vocabulários utilizados.

**Recuperação dos dados DATAtourisme**
Os dados públicos estão disponíveis na [plataforma nacional de dados abertos] (https://www.data.gouv.fr/fr/datasets/datatourisme-la-base-nationale-des-donnees-du-tourisme-en-open-data/) e na [plataforma de difusão DATAtourisme] (https://diffuseur.datatourisme.fr/fr/login). Nesta última, cada utilizador pode configurar intercâmbios de dados entre a DATAtourisme e as outras aplicações: pedidos com vários critérios e formatos, como JSON, XML, RDF... 

**Tratamento dos dados DATAtourisme**
A fim de facilitar a reutilização dos dados, pode encontrar no portal [DATAtourisme Gitlab] (https://gitlab.adullact.net/adntourisme/datatourisme/) dois outros repositórios. A [API] (https://gitlab.adullact.net/adntourisme/datatourisme/api) fornece uma biblioteca PHP para pesquisar dados provenientes da DATAtourisme. O [ Docker Stack] (https://gitlab.adullact.net/adntourisme/datatourisme/docker-stack) disponibiliza um ambiente técnico que permite armazenar os dados da DATAtourisme em formato RDF.

**Comunidade DATAtourisme e Apoio**
A DATAtourisme é um projeto colaborativo desenhado de forma a poder ser enriquecido com os contributos dos seus utilizadores. Agradecemos os seus comentários e sugestões, de preferência diretamente na [página de apoio da DATAtourisme] (https://support.datatourisme.fr/).
