DATAtourisme is a national resource managed by [ADN tourisme](https://www.adn-tourisme.fr/) in collaboration with the French government. This resource consists of an ontology and a platform for the aggregation, standardisation and dissemination of Open Data in the tourism sector.
This ontology aims to structure **data describing all elements of tourist interest** collected by local tourist offices, departmental bodies and regional tourism committees: Festivals and events, heritage sites, leisure activities, itineraries, accommodation, restaurants, shops and services. This reference format is used in particular by [**the national platform DATAtourisme**](https://www.datatourisme.fr/) for the dissemination of public Open Data in the field of tourism relating to all French destinations: Data enters the platform in different formats and is then structured starting from the ontology in order to disseminate it as Open Data in a unified and interoperable format.

**Documentation on the DATAtourisme format**.
The documentation is [available online](https://www.datatourisme.fr/ontology/core/), and [can be downloaded from the Gitlab repository](https://gitlab.adullact.net/adntourisme/datatourisme/ontology). It is accompanied by documentation in PDF format with concrete examples and by CSV files of the various nomenclatures used and a glossary.

**Recovery of data from DATAtourisme**.
The national data are available on the [French public data open platform](https://www.data.gouv.fr/fr/datasets/datatourisme-la-base-nationale-des-donnees-du-tourisme-en-open-data/) and on the [DATAtourisme dissemination platform](https://diffuseur.datatourisme.fr/fr/login). This last platform allows you to set up the exchange of data streams between DATAtourisme and your applications: Multi-criteria queries, available formats Json, Xml, RDF, etc. 

**Use of data from DATAtourisme**.
To facilitate the reuse of data, you will find two other repositories in the [DATAtourisme Gitlab]( https://gitlab.adullact.net/adntourisme/datatourisme) section. The [API repository]( https://gitlab.adullact.net/adntourisme/datatourisme/api) offers you a PHP library with which you can query data from DATAtourisme. In the [Docker Stack Repository](https://gitlab.adullact.net/adntourisme/datatourisme/docker-stack) you will find a technical environment to store the data coming from DATAtourisme in RDF format.

**DATAtourisme Community and Support**.
DATAtourisme is a collaborative project that aims to be enriched by the contributions of its users. Please share your comments and suggestions preferably directly on the [DATAtourisme support page](https://support.datatourisme.fr/).
