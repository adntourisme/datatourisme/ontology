DATAtourisme è un programma nazionale sostenuto da [ADN tourisme] (https://www.adn-tourisme.fr/). ADN tourisme è la Federazione nazionale delle organizzazioni istituzionali del turismo in Francia. 
Consiste in un'ontologia e in una piattaforma di aggregazione, standardizzazione e diffusione sotto forma di dati aperti (open data), applicata al settore del turismo.
Questa ontologia intende **strutturare i dati che descrivono tutti i punti di interesse turistici** individuati dagli uffici del turismo, dalle agenzie dipartimentali e dalle commissioni regionali per il turismo: le feste e le manifestazioni, i siti del patrimonio culturale, le attività ricreative, gli itinerari, gli alloggi, i ristoranti, i negozi e i servizi. Questo formato di riferimento è utilizzato in particolare da [**la piattaforma nazionale DATAtourisme**] (https://www.datatourisme.fr/) per diffondere informazioni turistiche pubbliche su tutte le destinazioni francesi sotto forma di dati aperti: i dati sono ricevuti sulla piattaforma in diversi formati e quindi allineati all'ontologia in modo da poter essere distribuiti come dati aperti in un formato uniforme e interoperabile.
Questa ontologia è destinata ad essere rilevante per qualsiasi produttore di dati, su qualsiasi territorio anche al di fuori della Francia, e ad essere ampiamente condivisa.

**Documentazione sul formato DATAtourisme**
La documentazione è [disponibile online] (https://www.datatourisme.fr/ontology/core/) e [scaricabile dall'archivio Gitlab] (https://gitlab.adullact.net/adntourisme/datatourisme/ontology). È accompagnata da documentazione in formato PDF con esempi concreti e i file CSV delle diverse nomenclature e dei vocabolari utilizzati.

**Recupero dei dati DATAtourisme**
I dati nazionali sono disponibili sulla [piattaforma pubblica aperta francese di dati] (https://www.data.gouv.fr/fr/datasets/datatourisme-la-base-nationale-des-donnees-du-tourisme-en-open-data/) e sulla [piattaforma di diffusione DATAtourisme] (https://diffuseur.datatourisme.fr/fr/login). Su quest'ultima piattaforma è possibile configurare degli scambi di flussi tra DATAtourisme e le proprie applicazioni: interrogazioni con più criteri, formati disponibili Json, Xml, RDF, ecc. 

**Sfruttamento dei dati DATAtourisme**
Per facilitare il riutilizzo dei dati, nello spazio [DATAtourisme Gitlab] (https://gitlab.adullact.net/adntourisme/datatourisme/) sono presenti altri due archivi. L'[archivio API] (https://gitlab.adullact.net/adntourisme/datatourisme/api) offre una libreria PHP per interrogare i dati di DATAtourisme. In [archivio docker-stack] (https://gitlab.adullact.net/adntourisme/datatourisme/docker-stack) è disponibile un ambiente tecnico per la conservazione dei dati DATAtourisme in formato RDF.

**Comunità DATAtourisme e supporto**
DATAtourisme è un progetto collaborativo che si arricchisce grazie al contributo degli utenti. Invitiamo a condividere le proprie osservazioni e suggerimenti direttamente sul [sito di assistenza DATAtourisme] (https://support.datatourisme.fr/).
