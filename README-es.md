DATAtourisme es un programa nacional gestionado por [ADN tourisme](https://www.adn-tourisme.fr/). ADN tourisme es la Federación Nacional de organizaciones institucionales de turismo en Francia.
Se trata de un dispositivo compuesto por una ontología y una plataforma de agregación, normalización y difusión como datos abiertos, aplicados al sector turístico.
Esta ontología tiene por objeto **estructurar los datos que describen todos los puntos de interés turístico** identificados por las oficinas de turismo, las agencias departamentales y los comités regionales de turismo: festivales y eventos, lugares catalogados como patrimonio cultural, actividades de ocio, rutas, alojamientos, restaurantes, tiendas y servicios. Este formato de referencia es utilizado, en particular, por [**la plataforma nacional DATAtourisme**] (https://www.datatourisme.fr/) para la difusión abierta de información turística pública sobre los diferentes destinos franceses: la plataforma recibe los datos en diferentes formatos y, a continuación, se ajustan a la ontología, con el fin de difundirlos como datos abiertos, en un formato uniforme e interoperable.
Esta ontología pretende ser relevante para cualquier productor de datos, en cualquier territorio incluso fuera de Francia, y ser compartida ampliamente.

**Documentación sobre el formato DATAtourisme**
La documentación se encuentra [disponible en línea] (https://www.datatourisme.fr/ontology/core/) y [se puede descargar desde el repositorio de Gitlab] (https://gitlab.adullact.net/adntourisme/datatourisme/ontology). Además, va acompañada de un documento en formato PDF con ejemplos concretos y ficheros CSV de las diferentes nomenclaturas y vocabularios utilizados.

**Recuperación de los datos de DATAtourisme**
Los datos nacionales se encuentran disponibles en la [plataforma abierta de datos públicos franceses] (https://www.data.gouv.fr/fr/datasets/datatourisme-la-base-nationale-des-donnees-du-tourisme-en-open-data/) y en la [plataforma de difusión DATAtourisme] (https://diffuseur.datatourisme.fr/fr/login). En esta última plataforma, podrá configurar intercambios de flujos de datos entre DATAtourisme y sus aplicaciones: consultas multicriterio, formatos disponibles Json, XML, RDF, ETC. 

**Utilización de los datos de DATAtourisme**
A fin de facilitar la reutilización de los datos, encontrará otros dos repositorios en el espacio de [DATAtourisme Gitlab] (https://gitlab.adullact.net/adntourisme/datatourisme/ontology). El [repositorio API] (https://gitlab.adullact.net/adntourisme/datatourisme/api) pone a su disposición una biblioteca PHP que le permitirá consultar los datos de DATAtourisme. Por su parte, en el [repositorio de docker-stack] (https://gitlab.adullact.net/adntourisme/datatourisme/docker-stack) encontrará un entorno técnico para almacenar datos DATAtourisme en formato RDF.

**Asistencia y comunidad de DATAtourisme**
DATAtourisme es un proyecto colaborativo, concebido para crecer y evolucionar gracias a las aportaciones de sus usuarios. Puede hacernos llegar sus comentarios y sugerencias directamente a través de [la página de asistencia de DATAtourisme] (https://support.datatourisme.fr/).
